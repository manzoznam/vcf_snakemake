configfile: "config.yaml"

import os

def get_output_directory(wildcards):
    # Extract the relevant parts from the input path
    dirs = os.path.dirname(wildcards)
    return f"{dirs}"

path_dir = get_output_directory(config['input_file'])
print(config['chr_remove'])
print(config['tabix_gnomad'])

rule all:
	input:
		f'{path_dir}/Module_gnomad_MAF/chr_removed.vcf',
		f'{path_dir}/Module_gnomad_MAF/gnomad_input.tsv',
		f'{path_dir}/Module_gnomad_MAF/gnomad_output.tsv',
		f'{path_dir}/Module_gnomad_MAF/R_join.log'

print("Starting gnomad section")
include: 'rules/gnomad.smk'



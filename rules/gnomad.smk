# Normalize the VCF file
import os

def get_output_directory(wildcards):
    # Extract the relevant parts from the input path
    dirs = os.path.dirname(wildcards)
    return f"{dirs}"

path_dir = get_output_directory(config['input_file'])

print("#### rules/gnomad.smk")
print("#### chr_remove")
print(config['chr_remove'])

print("#### tabix_gnomad") 
print(config['tabix_gnomad'])
join_R = "/home/watchdog/github_app/snakemake_VCF/R/join.R"



rule vcf_prep:
	input: input_file=config["input_file"]
	output: f'{path_dir}/Module_gnomad_MAF/chr_removed.vcf'
	run:
		shell("bcftools norm -m - {input} | bcftools annotate --rename-chrs {config[chr_remove]} > {output}")

rule gnomad_input:
	input: f'{path_dir}/Module_gnomad_MAF/chr_removed.vcf'
	output: f'{path_dir}/Module_gnomad_MAF/gnomad_input.tsv'
	run:
		shell('bcftools query -f "%CHROM\t%POS\t%REF\t%ALT" {input} > {output}')


rule gnomad_output:
	input: f'{path_dir}/Module_gnomad_MAF/gnomad_input.tsv'
	output: f'{path_dir}/Module_gnomad_MAF/gnomad_output.tsv'
	run: 
		shell("tabix -R {input} {config[tabix_gnomad]} > {output}")


rule annotate_vcf:
        input: 	gin=f'{path_dir}/Module_gnomad_MAF/gnomad_input.tsv',
         	gout=f'{path_dir}/Module_gnomad_MAF/gnomad_output.tsv'
	output: f'{path_dir}/Module_gnomad_MAF/R_join.log'
	run:
		shell("Rscript {join_R} --gnomadin {input.gin} --gnomadout {input.gout} > {output}")





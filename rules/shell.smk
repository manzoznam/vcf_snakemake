# Define the output directory

rule create_gnomad_MAF_dir:
    output:
        directory("Module_gnomadMAF")
    shell:
        "mkdir -p {output}"

# Normalize the VCF file
rule normalize_vcf:
    input:
        vcf=config["input_vcf"]
    output:
        norm_vcf="Module_gnomadMAF/norm.vcf"
    shell:
        "bcftools norm -m - {input.vcf} > {output.norm_vcf}"

# Filter the VCF file to exclude entries with AF=0 and output in compressed format
rule filter_vcf:
    input:
        norm_vcf="Module_gnomadMAF/norm.vcf"
    output:
        filter_vcf="Module_gnomadMAF/norm.filter.vcf"
    shell:
        "bcftools view -i 'INFO/AF>{config[threshold_AF]}' {input.norm_vcf} -Oz -o {output.filter_vcf}"

# Annotate the VCF file with chromosome renaming
rule annotate_chr:
    input:
        filter_vcf="Module_gnomadMAF/norm.filter.vcf"
    output:
        chr_rem_vcf="Module_gnomadMAF/norm.filter.chr_removed.vcf"
    shell:
        "bcftools annotate --rename-chrs chr_remove.txt {input.filter_vcf} > {output.chr_rem_vcf}"

# Query the VCF file to create a tab-delimited file
rule create_tab:
    input:
        chr_rem_vcf="Module_gnomadMAF/norm.filter.chr_removed.vcf"
    output:
        input_tab="Module_gnomadMAF/input.tsv"
    shell:
        "bcftools query -f '%CHROM\t%POS\t%REF\t%ALT' {input.chr_rem_vcf} > {output.input_tab}"

rule create_gnomad:
    input:
        input_tab="Module_gnomadMAF/input.tsv"
    output:
        gnomad_tab="Module_gnomadMAF/gnomad_output.tsv"
    shell:
        "tabix -R {input.input_tab} {config[tabix_gnomadexomes]} > {output.gnomad_tab}"

# Annotate the VCF file with gnomAD MAF
rule annotate_maf:
    input:
        filter_vcf="Module_gnomadMAF/norm.filter.vcf.gz",
        hdr="gnomad.hdr"
    output:
        maf_vcf="Module_gnomadMAF/maf.pyrepair.vcf"
    shell:
        "bcftools annotate -a Annotfile.tsv.gz -h {input.hdr} -c CHROM,POS,REF,ALT,gnomad_MAF {input.filter_vcf} > {output.maf_vcf}"

# Filter the VCF file by gnomAD MAF
rule filter_maf:
    input:
        maf_vcf="Module_gnomadMAF/maf.pyrepair.vcf"
    output:
        gnomad_maf_vcf="Module_gnomadMAF/gnomad_MAF.pyrepair.vcf.gz"
    shell:
        "bcftools view -i 'INFO/gnomad_MAF<={config[threshold_gnomadMAF]}' {input.maf_vcf} -Oz -o {output.gnomad_maf_vcf}"



# Rule all specifies the final target files
rule all:
    input:
        rules.create_gnomad_MAF_dir.output,
        "Module_gnomadMAF/norm.vcf",
        "Module_gnomadMAF/norm.filter.vcf.gz",
        "Module_gnomadMAF/norm.filter.chr_removed.vcf",
        "Module_gnomadMAF/input.tsv"





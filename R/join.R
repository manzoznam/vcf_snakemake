#!/usr/bin/R

library(optparse)

option_list = list(
make_option(c("-i", "--gnomadin"), type="character", default=NULL,
              help="coordinates in", metavar="character"),
make_option(c("-o", "--gnomadout"), type="character", default=NULL,
              help="gnomad out with MAF", metavar="character"))

opt_parser = OptionParser(option_list=option_list);
opt = parse_args(opt_parser)

###### /optparse

input = dplyr::distinct(readr::read_tsv(opt$gnomadin, col_names = FALSE))
output = dplyr::distinct(readr::read_tsv(opt$gnomadout, col_names = FALSE))

colnames(input) = c("CHROM", "POS", "REF", "ALT")
colnames(output) = c("CHROM", "POS", "REF", "ALT", "gnomad_MAF")

fj = dplyr::left_join(input, output)
fj = dplyr::mutate(fj, gnomad_MAF = ifelse(is.na(gnomad_MAF),0, gnomad_MAF))
fj = dplyr::distinct(fj)
fj$CHROM = paste0("chr", fj$CHROM)
colnames(fj)[1] = "#CHROM"


outputdir= dirname(opt$gnomadin)
readr::write_tsv(fj,paste0(outputdir,"/Annotfile.tsv"))

